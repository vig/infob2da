---
layout: lab
exclude: true
---

<img src="cs171-logo.png" width="200">

&nbsp;

# Homework 0 - Course Enrollment

Welcome to CS171!  This initial homework intends to get you acquainted with the structure and set-up of CS171 to ensure that everything goes smoothly moving forward. Please complete this homework to gain a spot in the enrollment lottery! And do not worry, future homeworks will be a lot more exciting and focused on creating interactive web-based visualizations! :-)

To complete this homework, you will have to do the following steps (which will be explained in more detail below):

1. Get acquainted with the general course structure. 
2. Take the initial programming quiz.
3. Introduce yourself on Piazza.
4. Set-up Webstorm.
5. Fill out our enrollment survey.

## 1. Course Structure
**Please take a look at our syllabus and our weekly schedule overview on our [Canvas site](https://canvas.harvard.edu/courses/74834)! Most of your questions will be answered there!**

We will always post all materials and assignments for each week as a single module on Canvas (by Friday of the preceeding week).

### Class Meeting Times (live and mandatory):

All class meetings will be held in Zoom. Please make sure that you have a working microphone, have turned on your video, and are in a safe and quiet environment.

**Lab:** Mondays. You can pick a time that works best for you, either 9 am, 10:30 am, 3 pm, or 7:30 pm ET. (If Monday is a holiday, labs will be held on Tuesday instead.)

**Live Lecture:** Wednesdays. You can pick from two available times, either 9 am or 4:30 pm ET. 
 
Classes start on time!

*Lab note:*

* Labs are mostly self-guided programming tutorials. Labs are intended to be completed during class time. If you cannot finish it in time during class, please finish it at home. It is also a good idea to get started with the lab before actual class time. Labs always have to be submitted together with that week's homework.


### Work done at home:

* In preparation for class each week, you will work on your own through asynchronous materials (YouTube videos, websites, lecture videos, book chapters, etc.) to ensure that you are prepared for the programming labs and the activities in class. 
* Before the beginning of each class, you will have to complete a brief quiz that tests your understanding of the asynchronous materials for that week.
* Homework assignments are handed out on Mondays and are due the following Sunday at 11.59pm ET.

*Important:* 
The first lab is on Tuesday Sept. 8th (Monday Sept. 7th is a holiday). For the first lab, there is already a pre-reading and quiz to complete. Pre-readings, and pre-reading quizzes are accessible on the weekly modules in Canvas.

### Help, questions, feedback:

If you have any questions (administrative or visualization-related), there are several ways to reach out. Please follow the order below:

1. Use our online message board Piazza. Your message can be published either just to the teaching fellows, or the entire course. 
2. If you can wait until the next class meeting, ask course staff at the end of class.
3. Attend office hours. Office hours will be posted on [Canvas](https://canvas.harvard.edu/courses/74834/pages/schedule).
4. Email course staff, either the TF that is mentoring your project team, or staff@cs171.org. 



## 2. Programming Quiz
Please fill out our programming quiz on Canvas (under HW 0 of the Shopping Week module). This quiz covers very simple programming concepts and should only take you a couple of minutes. If you have trouble with this quiz, we strongly encourage you to take a programming class like CS50 prior to taking CS171.

<!--
## 4. Studio sign-up
Our studios start already in the third week of the semester, so it is vital that you give us your studio preferences as soon as possible! To sign-up, go to Canvas -> People -> Groups or use [this link](https://canvas.harvard.edu/courses/30002/groups).
-->

<!-- [DCE] 
On-campus students (non DCE), please follow the FAS Sectioning instructions [here](http://about.my.harvard.edu/sectioning-students) to sign up for studios.

DCE students will be assigned into studio groups by us, and will get an email from their assigned studio TF by Tuesday of the second week.
-->

## 3. Piazza
Click on the Piazza link on our Canvas page or go to [Piazza](https://piazza.com/class/kd0jcr9kf455yu) and sign up for the class using your Harvard e-mail address. 

<!-- [DCE]
If you are a DCE student and don't have a Harvard e-mail address, please sign up for one [here](http://g.harvard.edu/extension-school-and-summer-school-faq/extension_opt_in).-->

We will use Piazza as a forum to: discuss, find team members, arrange appointments, and ask questions. Piazza should be your primary form of communication with the staff. Use the staff e-mail only for individual requests, e.g., to excuse yourself from a mandatory guest lecture. We will also use Piazza to announce important information like room changes, etc.

### Introduction

Once you are signed up to the Piazza course forum, please read the posting guidelines and then introduce yourself to your classmates and course staff. Please post as a follow-up in the 'Introduction' thread and include your name/nickname, and tell us something interesting about yourself (e.g., an unusual hobby, past travels, or a cool project you did, etc.). 

## 4. Set up Webstorm

JetBrains WebStorm is a JavaScript IDE that we encourage you to use for D3 development in this class. You can download WebStorm [here](https://www.jetbrains.com/webstorm/).

You can claim a free student license for use in this class [here](https://www.jetbrains.com/student/). Make sure that you use your university email address for this step!

## 5. Enrollment Survey
Please complete the enrollment survey on Canvas (shopping week). It should only take a few minutes to complete and it will automatically enter you into the CS171 lottery.


<!--
### Vocareum (TODO)
You can access Vocareum through Canvas, by clicking on the specific homework assignment.

Vocareum allows you to upload files, as well as to directly edit code in your browser. We strongly suggest that you write the code locally on your computer, using an editor or IDE like WebStorm, and only upload your files once you are done with your implementation.
You can run your code in Vocareum, and this is also the visualization that we will grade. So you should always make sure that your uploaded code behaves as you expect it to!

Vocareum allows us to create galleries for each submitted homework, which allows you to explore the visualizations of others and to give peer feedback (after the deadline).

For HW0, we ask you to:

* Go into HW0 in Vocareum ('My Work').
* Upload a simple text file called 'readme.txt' with your name in it. You can still edit the file after uploading it, it will be saved automatically by Vocareum.
* Specify a thumbnail picture for your submission (under 'Actions'/'Upload gallery thumbnail'). Please choose an image or screenshot of a visualization of your choice (e.g., a bar chart, a new york times visual).
* Submit your homework by clicking on the 'Submit' button. After a successful submission, your homework will also appear in the 'LatestSubmission' directory on the left side of the Vocareum page. Here you can double check your work. If you still need to make chances, you can do that in the 'work' directory and re-submit the homework afterwards.

You can find more help for Vocareum [here](http://help.vocareum.com/article/30-getting-started-students).

Generally, we will always provide you with a directory structure on Vocareum, but you will have to upload all the files of your D3 project. (You can also zip your entire directory and upload it at once, it will automatically be decompressed. Alternatively you can link git to Vocareum.)
Also, please note that other students will be able to see your homework submission after the deadline has passed. We think that it is crucial for learning that you are exposed to other people's code and designs. Feedback is an invaluable tool for improving yourself!
-->

## Well done, welcome to CS 171!
